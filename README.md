# State of Libre

This repo exists to track where there is a software need that is not met with libre (or at all) or where current options are grossly inadequate. Simply open an issue or comment on a relevant existing one. 

Issues related to a specific project should be filed there. Issues that are directly relevant to LibreMiami should be filed in the LibreMiami meta repo.


